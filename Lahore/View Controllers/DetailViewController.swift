//
//  DetailViewController.swift
//  Lahore
//
//  Created by Huda Jawed on 7/29/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import DropDown
import Firebase
import FirebaseDatabase
import STZPopupView
import Cosmos

protocol FavoritesDelegate
{
    func updateFavorites()
}


class DetailViewController: UIViewController
{
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var verticalSpaceLocationLabel: NSLayoutConstraint!
    @IBOutlet weak var timeLabelHeight2: NSLayoutConstraint!
    @IBOutlet weak var timeLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var addressLabelHeight2: NSLayoutConstraint!
    @IBOutlet weak var addressLabelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var timingsLabel: UILabel!
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var dropView: UIView!
    var delegate : FavoritesDelegate?
    
    var typeId = 0
    var placeName = ""
    var placeDetail = ""
    var placeLocation = ""
    var timings = ""
    var address = ""
    var placeImages = [UIImage]()
    let dropdown = DropDown()
    var favorites = [String]()
    var dataModel = PlacesInfoViewModel()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = self.placeName
        self.PlaceLeftButton(image: BACK_IMAGE)
        self.PlaceRightButton(image: #imageLiteral(resourceName: "moreItem"))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.detailLabel.text = self.placeDetail
        self.addressLabel.text = self.address
        self.timingsLabel.text = timings
        collectionView.delegate = self
        collectionView.dataSource = self
        
        self.dataModel.getRatings(place: self.placeName)
        self.dataModel.ratingDelegate = self
        
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 14),
            .foregroundColor: UIColor.blue,
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "To find out the location, click here.",
                                                        attributes: attributes)
        locationBtn.setAttributedTitle(attributeString, for: .normal)
        
        DropDown.configureDropDown(dropDown: self.dropdown, view: self.dropView)
        
        if typeId == 0
        {
            updateView()
        }
    }
    
    @objc override func didTapRightItem()
    {
        if self.favorites.contains(self.placeName)
        {
            self.dropdown.dataSource = ["Unmark favorites","Add ratings","Share"]
        }
        else
        {
            self.dropdown.dataSource = ["Mark favorites","Add ratings","Share"]
        }
        self.dropdown.show()
        self.dropdown.selectionAction =
            {
                [unowned self] (index: Int, item: String) in
                if index == 0
                {
                    if self.favorites.contains(self.placeName)
                    {
                        self.unmarkFavorites()
                    }
                    else
                    {
                        self.markFavorites()
                    }
                    
                }
                else if index == 1
                {
                    self.showRatingsView()
                }
                else
                {
                    self.share()
                }
                self.dropdown.hide()
        }
    }
    
    @IBAction func didTapLocationBtn(_ sender: Any)
    {
        UIApplication.shared.open(URL(string: self.placeLocation)!)
    }
    
    func updateView()
    {
        self.verticalSpaceLocationLabel.constant = 0.0
        self.timeLabelHeight2.constant = 0.0
        self.timeLabelHeight.constant = 0.0
        self.addressLabelHeight.constant = 0.0
        self.addressLabelHeight2.constant = 0.0
        self.contentViewHeight.constant = 310
        self.view.layoutIfNeeded()
    }
    
    func share()
    {
        var objectsToShare = [String]()
        objectsToShare.append(placeName)
        objectsToShare.append(placeDetail)
        objectsToShare.append(placeLocation)
        
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
        present(activityViewController, animated: true, completion: nil)
        
    }
    
    func showRatingsView()
    {
        let popupView = Bundle.main.loadNibNamed("RatingsView", owner: self, options: nil)?[0] as! RatingsView
        popupView.delegate = self
        self.presentPopupView(popupView)
    }
    
    func unmarkFavorites()
    {
        guard let uid = UserDefaults.standard.object(forKey: "userid") as? String else {return}
        
        self.startLoading(message: "Removing from favourites..")
        Database.database().reference().child("favorites").child(uid).child(self.placeName).removeValue
            { error,arg  in
                
                if error != nil
                {
                    self.stopLoading()
                    self.showBanner(title: "Error", subTitle: error!.localizedDescription, style: .danger)
                    return
                }
                
                self.stopLoading()
                self.showBanner(title: "Success", subTitle: "Unmarked from favorites", style: .success)
                self.delegate?.updateFavorites()
                for i in 0..<self.favorites.count
                {
                    if self.placeName == self.favorites[i]
                    {
                        self.favorites.remove(at: i)
                        break
                    }
                }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+4.0) {
            self.stopLoading()
        }
        
    }
    
    func markFavorites()
    {
        self.startLoading(message: "Adding to favourites..")
        guard let userid = UserDefaults.standard.object(forKey: "userid") as? String else {return}
        Database.database().reference().child("favorites").child(userid).updateChildValues([self.placeName:typeId == 0 ? "place" : "restaurant"], withCompletionBlock: { (error, ref) in
            
            if let error = error
            {
                self.stopLoading()
                self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
                return
            }
            
            self.stopLoading()
            self.showBanner(title: "Success", subTitle: "Marked to favorites", style: .success)
            self.delegate?.updateFavorites()
            self.favorites.append(self.placeName)
            
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now()+4.0) {
            self.stopLoading()
        }
    }
    
}

extension DetailViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.placeImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlaceImagesCollectionViewCell", for: indexPath) as? PlaceImagesCollectionViewCell
        cell?.placeImageView.image = self.placeImages[indexPath.row]
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ImagesViewController") as? ImagesViewController
        vc?.imagesArray = self.placeImages
        vc?.index = indexPath.item
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return self.placeImages.count > 2 ? CGSize(width: self.collectionView.frame.size.width/3, height: self.collectionView.frame.size.height) :  CGSize(width: self.collectionView.frame.size.width/2, height: self.collectionView.frame.size.height)
    }
    
    
}

extension DetailViewController : RatingsDelegate
{
    func cancelBtn()
    {
        self.dismissPopupView()
    }
    
    func didRecieveRatings(ratings: Double)
    {
        self.ratingView.rating = ratings
        print(ratings)
    }
    
    func doneRating(ratings:Double)
    {
        self.startLoading(message: "Adding rating..")
        
        guard let userid = UserDefaults.standard.object(forKey: "userid") as? String else {return}
        Database.database().reference().child("ratings").child(self.placeName).updateChildValues([userid : ratings], withCompletionBlock: { (error, ref) in
            
            if let error = error
            {
                self.stopLoading()
                self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
                return
            }
            
            self.stopLoading()
            self.showBanner(title: "Success", subTitle: "Rating has added", style: .success)
            self.dataModel.getRatings(place: self.placeName)
            self.dismissPopupView()
            
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now()+4.0) {
            self.stopLoading()
        }
        
    }
    
    
}


