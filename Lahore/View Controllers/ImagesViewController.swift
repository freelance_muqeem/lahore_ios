//
//  ImagesViewController.swift
//  Lahore
//
//  Created by Huda Jawed on 7/29/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit

class ImagesViewController: UIViewController
{
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    var imagesArray = [UIImage]()
    var index = 0
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.PlaceLeftButton(image: BACK_IMAGE)
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        self.pageController.numberOfPages = self.imagesArray.count
     
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        self.collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredHorizontally, animated: false)
         self.pageController.currentPage = index
    }

  
}


extension ImagesViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as? ImageCollectionViewCell
        cell?.placeImageView.image = self.imagesArray[indexPath.row]
        return cell!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: self.collectionView.frame.size.width, height: self.collectionView.frame.size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
       
        if let indexpath = self.collectionView.indexPathsForVisibleItems.first
        {
            self.pageController.currentPage = indexpath.item
        }
    }
    
    
}
