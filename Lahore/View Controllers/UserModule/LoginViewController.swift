//
//  LoginViewController.swift
//  Lahore
//
//  Created by Abdul Muqeem on 01/08/2019.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import NotificationBannerSwift

class LoginViewController: UIViewController
{

    @IBOutlet weak var loginTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var LoginLabel: UILabel!
    @IBOutlet weak var confirmPassword: UIImageView!
    @IBOutlet weak var nameImageView: UIImageView!
    @IBOutlet weak var loginVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var signupBtn: CustomButton!
    @IBOutlet weak var loginBtn: CustomButton!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    
    var isLogin = true
    
    class func instantiateFromStoryboard() -> LoginViewController
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LoginViewController
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.title = "Login"
        self.navigationController?.ShowNavigationBar()
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.navigationController?.navigationBar.isHidden = true
        self.view.setGradientBackground(colorTop: UIColor(named: "ThemeColor")!, colorBottom: UIColor(named: "SecondaryColor")!)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.nameView.alpha =  0.0
        self.nameField.alpha = 0.0
        self.confirmPasswordField.alpha = 0.0
        self.passwordView.alpha = 0.0
        self.confirmPassword.alpha = 0.0
        self.nameImageView.alpha = 0.0
        self.loginTopConstraint.constant = 45.0
        self.loginVerticalConstraint.constant = 0.0
    }
    
     func homeAction()
    {
        self.view.endEditing(true)
        
        let controller = SideMenuRootViewController.instantiateFromStoryboard()
        controller.leftViewPresentationStyle = .slideAbove
        AppDelegate.getInstatnce().window?.rootViewController = controller
        
    }
    
    func updateViews()
    {
        
        self.nameField.text = ""
        self.emailField.text = ""
        self.passwordField.text = ""
        self.confirmPasswordField.text = ""
        
        self.loginBtn.backgroundColor = self.isLogin ? UIColor(named: "SecondaryColor")! : .clear
        self.signupBtn.backgroundColor =  self.isLogin ? .clear : UIColor(named: "ThemeColor")!
        self.loginBtn.setTitleColor( self.isLogin ? .white : UIColor(named: "SecondaryColor")! , for: .normal)
        self.signupBtn.setTitleColor( self.isLogin ? UIColor(named: "ThemeColor")! : .white , for: .normal)
        self.LoginLabel.text =  self.isLogin ? "Login" : "Signup"
        
        if isLogin
        {
            UIView.animate(withDuration: 0.3, animations:
                {
                    self.nameView.alpha = 0.0
                    self.nameField.alpha = 0.0
                    self.confirmPasswordField.alpha = 0.0
                    self.passwordView.alpha = 0.0
                    self.confirmPassword.alpha = 0.0
                    self.nameImageView.alpha = 0.0
            })
            { (success) in
                UIView.animate(withDuration: 0.3)
                {
                    self.loginVerticalConstraint.constant =  0.0
                    self.loginTopConstraint.constant = 45.0
                    self.view.layoutIfNeeded()
                }
            }
        }
        else
        {
            UIView.animate(withDuration: 0.3, animations:
                {
                    self.loginVerticalConstraint.constant =  40.0
                    self.loginTopConstraint.constant = 5.0
                    self.view.layoutIfNeeded()
            })
            { (success) in
                UIView.animate(withDuration: 0.3)
                {
                    self.nameView.alpha = 1.0
                    self.nameField.alpha = 1.0
                    self.confirmPasswordField.alpha = 1.0
                    self.passwordView.alpha = 1.0
                    self.confirmPassword.alpha = 1.0
                    self.nameImageView.alpha = 1.0
                }
            }
        }
        
    }
    
    
    func createUser()
    {
        if !self.isValidUser()
        {
            return
        }
        
         self.startLoading(message: "Signing up")
         Auth.auth().createUser(withEmail:self.emailField.text!, password:self.passwordField.text!) { (result, error) in
         
         if let error = error
         {
            self.stopLoading()
            self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
           return
         }
         
         guard let uid = result?.user.uid else { return }
         
            let values = ["email":self.emailField.text!, "username":self.nameField.text! ]
            Database.database().reference().child("users").child(uid).updateChildValues(values, withCompletionBlock: { (error, ref) in
                
                if let error = error
                {
                     self.stopLoading()
                    self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
                    return
                }
                 self.stopLoading()
                self.showBanner(title: "Congratulations!", subTitle: "Your account has been created successfully", style: .success)
                self.homeAction()
            })
            
         }
        
    }
    
    func logUserIn()
    {
        if self.emailField.text! == "" || self.passwordField.text == ""
        {
            self.showBanner(title: "Error", subTitle: "Kindly fill all the requirments", style: .danger)
            return
        }
        
        self.startLoading(message: "Logging in")
        Auth.auth().signIn(withEmail: self.emailField.text!, password: self.passwordField.text!) { (result, error) in
            
            if let error = error
            {
                self.stopLoading()
                self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
                return
            }
            
            self.stopLoading()
            self.showBanner(title: "Congratulations!", subTitle: "You are logged in successfully", style: .success)
            self.homeAction()
            
        }
    }
    
    func isValidUser()->Bool
    {
        
        if self.nameField.text == "" || self.emailField.text == "" || self.passwordField.text == "" || self.confirmPasswordField.text == ""
        {
             self.showBanner(title: "Error", subTitle: "Kindly fill all the requirments", style: .danger)
            return false
        }
        else if !AppHelper.isValidEmail(testStr: self.emailField.text!)
        {
        self.showBanner(title: "Error", subTitle: "Invalid Email", style: .danger)
              return false
        }
        else if self.passwordField.text != self.confirmPasswordField.text!
        {
             self.showBanner(title: "Error", subTitle: "Passwords does not matched", style: .danger)
            return false
        }
        
        return true
        
    }
    
    //MARK:- Event handlers
    
    @IBAction func didTapLoginButton(_ sender: Any)
    {
        if isLogin
        {
           self.logUserIn()
        }
        else
        {
             isLogin = true
            updateViews()
        }
    }
    
    @IBAction func didTapSignupButton(_ sender: Any)
    {
        if isLogin
        {
            isLogin = false
             updateViews()
        }
        else
        {
            self.createUser()
        }
    }
    
    

}
