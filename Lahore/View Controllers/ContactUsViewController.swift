//
//  ContactUsViewController.swift
//  Lahore
//
//  Created by Huda Jawed on 8/3/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class ContactUsViewController: UIViewController {

    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    
    class func instantiateFromStoryboard() -> ContactUsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ContactUsViewController
    }
   
   
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.title = "Contact Us"
        self.PlaceLeftButton(image: MENU_IMAGE)
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        guard let username = UserDefaults.standard.object(forKey: "username") as? String else {return}
        guard let email = UserDefaults.standard.object(forKey: "email") as? String
            else {return}
        
        self.nameField.text = username
        self.emailField.text = email
        self.messageTextView.layer.cornerRadius = 5.0
        self.messageTextView.layer.borderWidth = 1.0
        self.messageTextView.layer.borderColor = UIColor.lightGray.cgColor
        
        
        self.view.setGradientBackground(colorTop: UIColor(named: "ThemeColor")!, colorBottom: UIColor(named: "SecondaryColor")!)
       
    }
    
    
    override func didTapLeftItem()
    {
        self.showLeftViewAnimated(self)
    }

    @IBAction func didTapSendButton(_ sender: Any)
    {
      if self.messageTextView.text! == ""
      {
         self.showBanner(title: "Error", subTitle: "Message can't be empty", style: .danger)
        return
      }
        
       self.startLoading(message: "Sending....")
        
        guard let uid = UserDefaults.standard.object(forKey: "userid") as? String else {return}
        
        let values = ["email":self.emailField.text!,"message":self.messageTextView.text! ]
        Database.database().reference().child("messages").child(uid).updateChildValues(values, withCompletionBlock: { (error, ref) in
            
            if let error = error
            {
                self.stopLoading()
                self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
                return
            }
            self.stopLoading()
            self.messageTextView.text = ""
            self.showBanner(title: "Success", subTitle: "Your query has been sent!", style: .success)
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now()+3.0)
        {
            self.stopLoading()
        }

    }
    
}
