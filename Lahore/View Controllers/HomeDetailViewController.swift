//
//  ViewController.swift
//  Lahore
//
//  Created by Huda Jawed on 7/29/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit

class HomeDetailViewController: UIViewController {

    class func instantiateFromStoryboard() -> HomeDetailViewController
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeDetailViewController
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    let dataModel = PlacesInfoViewModel()
    var placesArray = [PlacesInfoDataModel]()
    var restaurantsArray = [RestaurantsInfoDataModel]()
    var allPlacesData = [PlacesInfoDataModel]()
    var allRestaurantsData = [RestaurantsInfoDataModel]()
    var searchText = ""
    var typeId : Int = 0
    var favorites = [String]()
    var isFavorites = false
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
     
        if isFavorites
        {
             self.PlaceLeftButton(image: MENU_IMAGE )
        }
        else
        {
            self.PlaceLeftButton(image: BACK_IMAGE)
        }
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self

        self.startLoading(message: "Fetching data...")
        self.dataModel.delegate = self
        self.dataModel.getFavourites()
        if isFavorites
        {
             self.title = "Favorites"
             self.dataModel.fetchPlacesData()
             self.dataModel.fetchRestaurantsData()
        }
        else
        {
            self.title = typeId == 0 ? "Places" : "Restaurants"
            typeId == 0 ? self.dataModel.fetchPlacesData() :   self.dataModel.fetchRestaurantsData()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+4.0)
        {
            self.stopLoading()
        }
      
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if isFavorites
        {
           self.filterFavorites()
        }
    }
    
    override func didTapLeftItem()
    {
        if isFavorites
        {
        self.showLeftViewAnimated(self)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    @objc func readMoreBtn(_ sender : UIButton)
    {
        let section = self.tableView.indexPath(for:((sender.superview?.superview as? HomeTableViewCell)!))?.section
        
        let row = self.tableView.indexPath(for:((sender.superview?.superview as? HomeTableViewCell)!))?.row
        
        typeId = isFavorites ? section! : typeId
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController
        vc?.placeName = typeId == 0 ? ((self.placesArray[row!]).placeName ?? "") : ((self.restaurantsArray[row!]).restaurantName ?? "")
        vc?.placeDetail = typeId == 0 ? ((self.placesArray[row!]).placeDetail ?? "") : ((self.restaurantsArray[row!]).restaurantDetail ?? "")
        vc?.placeLocation = typeId == 0 ? ((self.placesArray[row!]).placeLocation ?? "") : ((self.restaurantsArray[row!]).restaurantLocation ?? "")
        vc?.placeImages = typeId == 0 ? ((self.placesArray[row!]).placeImages ?? [UIImage]()) : ((self.restaurantsArray[row!]).restaurantImages ?? [UIImage]())
        vc?.timings = typeId == 0 ? "" : self.restaurantsArray[row!].restaurantTimings!
        vc?.address = typeId == 0 ? "" : self.restaurantsArray[row!].restaurantAddress!
        vc?.typeId = self.typeId
        vc?.favorites = self.favorites
        vc?.delegate = self
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func filterFavorites()
    {
        self.placesArray = [PlacesInfoDataModel]()
        self.restaurantsArray = [RestaurantsInfoDataModel]()
        
        for place in self.allPlacesData
        {
            if self.favorites.contains(place.placeName!)
            {
                self.placesArray.append(place)
            }
        }
        
        for restaurant in self.allRestaurantsData
        {
            if self.favorites.contains(restaurant.restaurantName!)
            {
                self.restaurantsArray.append(restaurant)
            }
        }
        self.tableView.reloadData()
    }
    
    func filterData()
    {
        if typeId == 0
        {
        for place in self.allPlacesData
        {
            if (((place.placeName)?.lowercased())!.contains(self.searchText.lowercased()))
            {
                self.placesArray.append(place)
            }
        }
        }
        else
        {
            for restaurant in self.allRestaurantsData
            {
                if (((restaurant.restaurantName)?.lowercased())!.contains(self.searchText.lowercased()))
                {
                    self.restaurantsArray.append(restaurant)
                }
                
            }
        }
          self.tableView.reloadData()
    }


}

extension HomeDetailViewController : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return isFavorites ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  isFavorites ? ((section == 0 ? (self.placesArray.count) : (self.restaurantsArray.count))) :  (typeId == 0 ? (self.placesArray.count) : (self.restaurantsArray.count))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as? HomeTableViewCell
        cell?.placeNameLabel.text = isFavorites ? (indexPath.section == 0 ? self.placesArray[indexPath.row].placeName : self.restaurantsArray[indexPath.row].restaurantName) :  (typeId == 0 ? self.placesArray[indexPath.row].placeName : self.restaurantsArray[indexPath.row].restaurantName)
        cell?.placeDetailLabel.text = isFavorites ? (indexPath.section == 0 ? self.placesArray[indexPath.row].placeDetail :  self.restaurantsArray[indexPath.row].restaurantDetail) : (typeId == 0 ? self.placesArray[indexPath.row].placeDetail :  self.restaurantsArray[indexPath.row].restaurantDetail)
        cell?.placeImageView.image = isFavorites ? (indexPath.section == 0 ? self.placesArray[indexPath.row].placeImages?[0] : self.restaurantsArray[indexPath.row].restaurantImages?[0]) : (typeId == 0 ? self.placesArray[indexPath.row].placeImages?[0] : self.restaurantsArray[indexPath.row].restaurantImages?[0])
        cell?.readMoreBtn.addTarget(self, action: #selector(readMoreBtn(_:)), for: .touchUpInside)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerview  = UIView(frame: CGRect(x: 20, y: 0, width: self.view.frame.size.width-40, height: 30))
        headerview.backgroundColor = .clear
        let label = UILabel(frame: CGRect(x: 15, y: 10, width: self.view.frame.size.width, height: 20))
        label.text = section == 0 ? "Places" : "Restaurants"
        label.font = UIFont(name: "HeleveticaNeue", size: 28.0)
        label.textColor = UIColor(named: "SecondaryColor")
        headerview.addSubview(label)
        return headerview
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if isFavorites
        {
        if (section == 0 && self.placesArray.count != 0) || (section == 1 && self.restaurantsArray.count != 0)
        {
           return 30.0
        }
        }
        return 0
    }
    
    
}


extension HomeDetailViewController: dataDelegate
{

    func didRecieveFavorites(favourites: [String])
    {
        self.favorites = favourites
    }
    
   
    func didFailed()
    {
         self.showBanner(title: "Error", subTitle: "Failed to fetch data", style: .danger)
         self.stopLoading()
    }
    
    func didRecievePlacesData(data: [PlacesInfoDataModel])
    {
      
        if self.searchText != ""
        {
            self.allPlacesData = data
            self.filterData()
        }
        else
        {
            self.allPlacesData = data
            self.placesArray = data
        }
        if !isFavorites
        {
         
          self.tableView.reloadData()
        }
        else
        {
            filterFavorites()
        }
         self.stopLoading()
    }
    
    func didRecieveRestaurantsData(data: [RestaurantsInfoDataModel])
    {
     
        if self.searchText != ""
        {
            self.allRestaurantsData = data
            self.filterData()
        }
        else
        {
            self.allRestaurantsData = data
            self.restaurantsArray = data
        }
        if !isFavorites
        {
          self.tableView.reloadData()
        }
        else
        {
            filterFavorites()
        }
         self.stopLoading()
    }
    
}

extension HomeDetailViewController : FavoritesDelegate
{
    func updateFavorites()
    {
        self.dataModel.getFavourites()
    }
    
    
}

