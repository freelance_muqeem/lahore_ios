//
//  SideMenuViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 19/09/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class SideMenuViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SideMenuViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SideMenuViewController
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnVerifyEmail:UIButton!
    
    @IBOutlet weak var imgLogo:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblDegree:UILabel!
    
    var expireValue:String?
    let currentDate = Date()
    
    var itemsLogin: [String] = [ "Home" , "My Favourites" , "About Us" ,"Contact Us", "Privacy Policy"  , "Logout"]
    var imagesLogin: [UIImage] = [UIImage(named:"home_icon")! , UIImage(named:"heart_icon")! , UIImage(named:"about_icon")! ,UIImage(named:"contact_icon")!, UIImage(named:"privacy_icon")! , UIImage(named:"logout_icon")!]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Register Cell
        let cell = UINib(nibName:String(describing:SideMenuTableViewCell.self), bundle: nil)
        self.tableView.register(cell, forCellReuseIdentifier: String(describing: SideMenuTableViewCell.self))
        
        let ref = Database.database().reference()
        ref.child("users").observeSingleEvent(of: .value, with:
            { (data) in
                
                let user = data.value as? Dictionary<String,Any>
                if let userData = user?[(Auth.auth().currentUser?.uid)!] as? Dictionary<String,Any>
                {
                    self.lblName.text = userData["username"] as! String
                    UserDefaults.standard.set(userData["username"] as! String, forKey: "username")
                    UserDefaults.standard.set(userData["email"] as! String, forKey: "email")
                }
                
        })
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    func signout()
    {
        do
        {
            try Auth.auth().signOut()
            
            let nav = RootViewController.instantiateFromStoryboard()
            AppDelegate.getInstatnce().window?.rootViewController = nav
            let vc = LoginViewController.instantiateFromStoryboard()
            nav.pushViewController(vc, animated: true)
            
            UserDefaults.standard.set(nil, forKey: "userid")
            UserDefaults.standard.set(nil, forKey: "username")
            UserDefaults.standard.set(nil, forKey: "email")
        }
        catch let error
        {
            self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
        }
    }
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemsLogin.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(70)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SideMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuTableViewCell
        
        cell.lblTitle.text = self.itemsLogin[indexPath.row]
        cell.imgView.image = self.imagesLogin[indexPath.row]
        
        if indexPath.row == 5 {
            cell.lineView.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = HomeViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            
        }
        else if indexPath.row == 1
        {
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = HomeDetailViewController.instantiateFromStoryboard()
            vc.isFavorites = true
            navigationController.setViewControllers([vc], animated: false)
            
        }
        else if indexPath.row == 2
        {
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = AboutUsViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            
        }
        else if indexPath.row == 3
        {
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = ContactUsViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
        }
        else if indexPath.row == 4
        {
            UIApplication.shared.open(URL(string: "https://www.termsfeed.com/privacy-policy/50406381716cfec2413fe454266cd994")!)
        }
        else if indexPath.row == 5
        {
            self.signout()
        }
        self.hideLeftViewAnimated(self)
        
    }
    
}







