//
//  HomeViewController.swift
//  Lahore
//
//  Created by Abdul Muqeem on 01/08/2019.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import DropDown

extension HomeViewController: UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        self.searchTypeName.removeAll()
        
        if (textField.text?.isEmpty)! || textField.text == "" {
            self.searchTypeDropdown.hide()
        }
        else {
            
            self.searchTypeName.append( textField.text! + " in category " + "Places")
            self.searchTypeName.append( textField.text! + " in category " + "Restaurants")
            
            self.searchTypeDropdown.dataSource = self.searchTypeName
            self.searchTypeDropdown.show()
        }
    }
}

class HomeViewController: UIViewController {

    class func instantiateFromStoryboard() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
    }
    
    // Seacrh
    @IBOutlet weak var txtSearch:UITextField!
    var searchTypeDropdown = DropDown()
    var searchTypeName = [String]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Home"
        self.PlaceLeftButton(image: MENU_IMAGE )
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.txtSearch.delegate = self
        self.txtSearch.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
        // Search Category DropDown
        
        self.searchTypeDropdown.anchorView = self.txtSearch // UIView or UIBarButtonItem
        self.searchTypeDropdown.bottomOffset = CGPoint(x: 0, y:(searchTypeDropdown.anchorView?.plainView.bounds.height)!)
        self.searchTypeDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.view.endEditing(true)
            
            self.searchTypeName.removeAll()
            
            if index == 0
            {
                let vc = HomeDetailViewController.instantiateFromStoryboard()
                 vc.searchText = self.txtSearch.text!
                 vc.typeId = 0
                 vc.isFavorites = false
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                
                let vc = HomeDetailViewController.instantiateFromStoryboard()
                vc.searchText = self.txtSearch.text!
                vc.typeId = 1
                vc.isFavorites = false
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
    }
    
    override func didTapLeftItem()
    {
        self.showLeftViewAnimated(self)
    }
    
    @IBAction func searchAction( _ sender : UIButton)
    {
        
        if self.txtSearch.text!.isEmptyOrWhitespace()
        {
            self.showBanner(title: "Alert", subTitle: "Search can't be empty", style: .danger)
            return
        }
        
        let vc = HomeDetailViewController.instantiateFromStoryboard()
        vc.searchText = self.txtSearch.text!
        vc.typeId = 0
        vc.isFavorites = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapPlacesButton(_ sender: Any)
    {
        let vc = HomeDetailViewController.instantiateFromStoryboard()
        vc.typeId = 0
        vc.isFavorites = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func didTapRestaurantsButton(_ sender: Any)
    {
        let vc = HomeDetailViewController.instantiateFromStoryboard()
        vc.typeId = 1
        vc.isFavorites = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
