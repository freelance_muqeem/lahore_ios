//
//  AboutUsViewController.swift
//  Lahore
//
//  Created by Huda Jawed on 8/3/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {

    class func instantiateFromStoryboard() -> AboutUsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AboutUsViewController
    }
   
    @IBOutlet weak var detailLabel: UILabel!
    let detail = "Consider our app as your guide if you wanna explore Lahore, Get to know about famous and historical places/restaurants of Lahore through our app.\nYou can find detailed information, history, images, location and rating of famous places/restaurants in Lahore.\n\nOn the main page, either select places or restaurants to explore them, you can also search restaurant/places of your choice by searching them from searchbar, On the next screen, there will be a list of famous places/restaurants in Lahore. If you want to know detailed description about each place/restaurant, click on read more button where you can find detailed description, lots of images, ratings and location of that place. Not only this you can share how much do you like that place by adding your ratings and share your experience with others. If you like some places/restaurants and want to save it for future, you just need to add them on your favorites list so that you can view them later by clicking favorite button present on your side drawer.\n\nYou can also share location of your favorite places/restaurants to your friends and family.\n\nIf you have any query regarding our app , you can contact us by clicking 'Contact Us button present in the side drawer."
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.title = "About Us"
        self.PlaceLeftButton(image: MENU_IMAGE )
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.view.setGradientBackground(colorTop: UIColor(named: "ThemeColor")!, colorBottom: UIColor(named: "SecondaryColor")!)
        self.detailLabel.text = self.detail
    }
    
    override func didTapLeftItem()
    {
        self.showLeftViewAnimated(self)
    }


}
