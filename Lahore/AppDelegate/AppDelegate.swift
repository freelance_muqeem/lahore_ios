//
//  AppDelegate.swift
//  Lahore
//
//  Created by Huda Jawed on 7/29/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import LGSideMenuController
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        // Override point for customization after application launch.
        // UIApplication.shared.statusBarView?.backgroundColor = UIColor(named: "ThemeColor")!
         UIApplication.shared.statusBarStyle = .lightContent
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        
        if Auth.auth().currentUser == nil
        {
            self.NavigateTOInitialViewController()
        }
        else
        {
            UserDefaults.standard.set(Auth.auth().currentUser?.uid, forKey: "userid")
            self.NavigateToHomeViewController()
        }
        
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {
 
    }

    func applicationWillEnterForeground(_ application: UIApplication) {

    }

    func applicationDidBecomeActive(_ application: UIApplication) {

    }

    func applicationWillTerminate(_ application: UIApplication) {

    }

    static func getInstatnce() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func NavigateTOInitialViewController() {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let nav = RootViewController.instantiateFromStoryboard()
        self.window?.rootViewController = nav
        let vc = LoginViewController.instantiateFromStoryboard()
        nav.pushViewController(vc, animated: true)
        
    }

    func NavigateToHomeViewController()
    {
        let controller = SideMenuRootViewController.instantiateFromStoryboard()
        controller.leftViewPresentationStyle = .slideAbove
        AppDelegate.getInstatnce().window?.rootViewController = controller
    }
    
}

