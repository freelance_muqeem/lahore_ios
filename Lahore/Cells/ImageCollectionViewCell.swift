//
//  ImageCollectionViewCell.swift
//  Lahore
//
//  Created by Huda Jawed on 7/29/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var placeImageView: UIImageView!
    
}
