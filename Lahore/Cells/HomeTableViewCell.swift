
//
//  HomeTableViewCell.swift
//  Lahore
//
//  Created by Huda Jawed on 7/29/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var readMoreBtn: UIButton!
    @IBOutlet weak var placeDetailLabel: UILabel!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
