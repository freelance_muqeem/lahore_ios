//
//  SideMenuTableViewCell.swift
//  Syllabit
//
//  Created by Protege Global on 21/03/2018.
//  Copyright © 2018 Protege Global. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lineView:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
}
