//
//  PlacesInfoViewModel.swift
//  Lahore
//
//  Created by Huda Jawed on 7/29/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

protocol dataDelegate
{
    func didRecievePlacesData(data:[PlacesInfoDataModel])
    func didRecieveRestaurantsData(data:[RestaurantsInfoDataModel])
    func didRecieveFavorites(favourites:[String])
    func didFailed()
}

class PlacesInfoViewModel
{
    let imagesArray = [
        "Lahore Fort" : [#imageLiteral(resourceName: "lahoreFort"),#imageLiteral(resourceName: "lahoreFort2")],
        "Badshahi Mosque" : [#imageLiteral(resourceName: "badshahiMosque"),#imageLiteral(resourceName: "badshahiMosque2")],
        "Shalimar Bagh" : [#imageLiteral(resourceName: "shalimarBagh"),#imageLiteral(resourceName: "shalimarBagh2"),#imageLiteral(resourceName: "shalimarBagh3"),#imageLiteral(resourceName: "shalimarBagh4")],
        "Sheesh Mahal" : [#imageLiteral(resourceName: "sheeshMahal"),#imageLiteral(resourceName: "sheeshMahal2"),#imageLiteral(resourceName: "sheeshMahal3"),#imageLiteral(resourceName: "sheeshMahal4"),#imageLiteral(resourceName: "sheeshMahal5")],
        "Minar-e-Pakistan" : [#imageLiteral(resourceName: "minarPak"),#imageLiteral(resourceName: "minarPak2"),#imageLiteral(resourceName: "minarPak3"),#imageLiteral(resourceName: "minarPak4")],
        "Lahore Museum" : [#imageLiteral(resourceName: "lahoreMuseum"),#imageLiteral(resourceName: "lahoreMuseum2"),#imageLiteral(resourceName: "lahoreMuseum3"),#imageLiteral(resourceName: "lahoreMuseum4"),#imageLiteral(resourceName: "lahoreMuseum5")],
        "Tomb of Jahangir" : [#imageLiteral(resourceName: "tombOfJahangir"),#imageLiteral(resourceName: "tombOfJahangir2"),#imageLiteral(resourceName: "tombOfJahangir3")],
        "Masjid Wazir Khan" : [#imageLiteral(resourceName: "masjidWazirKhan"),#imageLiteral(resourceName: "masjidWazirKhan2"),#imageLiteral(resourceName: "masjidWazirKhan3"),#imageLiteral(resourceName: "masjidWazirKhan4")],
        "Lahore Zoo" : [#imageLiteral(resourceName: "lahoreZoo"),#imageLiteral(resourceName: "lahoreZoo2"),#imageLiteral(resourceName: "lahoreZoo3")],
        "Bagh-e-Jinnah" : [#imageLiteral(resourceName: "bagh-e-Jinnah"),#imageLiteral(resourceName: "bagh-e-Jinnah2"),#imageLiteral(resourceName: "bagh-e-Jinnah3"),#imageLiteral(resourceName: "bagh-e-Jinnah4"),#imageLiteral(resourceName: "bagh-e-Jinnah7"),#imageLiteral(resourceName: "bagh-e-Jinnah5"),#imageLiteral(resourceName: "bagh-e-Jinnah6")],
        "Grand Jamia Mosque" : [#imageLiteral(resourceName: "grandJamiaMosque"),#imageLiteral(resourceName: "grandJamiaMosque2"),#imageLiteral(resourceName: "grandJamiaMosque3"),#imageLiteral(resourceName: "grandJamiaMosque4")],
        "Chauburji" : [#imageLiteral(resourceName: "chauburji"),#imageLiteral(resourceName: "chauburji2")],
        "Shahi Hammam" : [#imageLiteral(resourceName: "shahiHammam"),#imageLiteral(resourceName: "shahiHammam2"),#imageLiteral(resourceName: "shahiHammam3"),#imageLiteral(resourceName: "shahiHammam4")],
        "Moti Masjid" : [#imageLiteral(resourceName: "motiMasjid"),#imageLiteral(resourceName: "motiMasjid2"),#imageLiteral(resourceName: "motiMasjid3"),#imageLiteral(resourceName: "motiMasjid4")],
        "Tomb of Allama Iqbal" : [#imageLiteral(resourceName: "tombOfAllamaIqbal"),#imageLiteral(resourceName: "tombOfAllamaIqbal2"),#imageLiteral(resourceName: "tombOfAllamaIqbal3")],
        "Food Street Fort Road" : [#imageLiteral(resourceName: "Food Street Fort Road"),#imageLiteral(resourceName: "Food Street Fort Road2")],
        "Baradari of Kamran Mirza" : [#imageLiteral(resourceName: "Baradari of Kamran Mirza"),#imageLiteral(resourceName: "Baradari of Kamran Mirza2"),#imageLiteral(resourceName: "Baradari of Kamran Mirza3"),#imageLiteral(resourceName: "Baradari of Kamran Mirza4")],
        "Faqir Khana Museum" : [#imageLiteral(resourceName: "Faqir Khana Museum"),#imageLiteral(resourceName: "Faqir Khana Museum2"),#imageLiteral(resourceName: "Faqir Khana Museum3"),#imageLiteral(resourceName: "Faqir Khana Museum4"),#imageLiteral(resourceName: "Faqir Khana Museum5")],
        "Naulakha" : [#imageLiteral(resourceName: "Naulakha"),#imageLiteral(resourceName: "Naulakha2"),#imageLiteral(resourceName: "Naulakha3")],
        "Lahori Gate" : [#imageLiteral(resourceName: "Lohari Gate"),#imageLiteral(resourceName: "Lohari Gate2")],
        "Sunehri Mosque" : [#imageLiteral(resourceName: "Sunehri Mosque"),#imageLiteral(resourceName: "Sunehri Mosque2"),#imageLiteral(resourceName: "Sunehri Mosque3")],
        "Qabail" : [#imageLiteral(resourceName: "Qabail"),#imageLiteral(resourceName: "Qabail2")],
        "Andaaz Restaurant" : [#imageLiteral(resourceName: "Andaaz"),#imageLiteral(resourceName: "Andaaz2")],
        "Monal" : [#imageLiteral(resourceName: "Monal")],
        "Salt'n Pepper Village Lahore" : [#imageLiteral(resourceName: "Salt"),#imageLiteral(resourceName: "Salt")],
        "Café Aylanto" : [#imageLiteral(resourceName: "Cafe"),#imageLiteral(resourceName: "Cafe2")],
        "Dar Marrakesh Restaurant" : [#imageLiteral(resourceName: "Dar"),#imageLiteral(resourceName: "Dar2")],
        "Cooco's Den" : [#imageLiteral(resourceName: "Cooco's Den"),#imageLiteral(resourceName: "Cooco's Den2")],
        "Café Zouk" : [#imageLiteral(resourceName: "Cafe Zouk2")]
        ]
    var delegate : dataDelegate?
    var ratingDelegate : RatingsDelegate?
    
    func fetchPlacesData()
    {
        var dataModelArray = [PlacesInfoDataModel]()
        let ref = Database.database().reference()
        ref.child("places").observeSingleEvent(of: .value) { (snapshot) in
            
            let dataDic = snapshot.value as? Dictionary<String,Any>
            for singleData in dataDic!
            {
                let data = singleData.value as? Dictionary<String,String>
                let item = PlacesInfoDataModel(placeName: singleData.key, placeDetail: data!["detail"]!, placeImages: self.imagesArray[singleData.key]!, placeLocation: data!["location"]!)
                dataModelArray.append(item)
            }
            self.delegate?.didRecievePlacesData(data: dataModelArray)
        }
        
        
    }
    
    func fetchRestaurantsData()
    {
        var dataModelArray = [RestaurantsInfoDataModel]()
        let ref = Database.database().reference()
        ref.child("restaurants").observeSingleEvent(of: .value) { (snapshot) in
            
            let dataDic = snapshot.value as? Dictionary<String,Any>
            for singleData in dataDic!
            {
                let data = singleData.value as? Dictionary<String,String>
                let item = RestaurantsInfoDataModel(restaurantName:  singleData.key, restaurantDetail: data!["detail"]!, restaurantImages: self.imagesArray[singleData.key]!, restaurantLocation: data!["location"]!, restaurantTimings: data!["timings"]!, restaurantAddress: data!["address"]!)
                dataModelArray.append(item)
            }
            self.delegate?.didRecieveRestaurantsData(data: dataModelArray)
            
        }
        
    }
    
    
    func getFavourites()
    {
        guard let userid = UserDefaults.standard.object(forKey: "userid") as? String else {return}
        var favourites = [String]()
        let ref = Database.database().reference()
        ref.child("favorites").observeSingleEvent(of: .value) { (snapshot) in
            
            if let dataDic = snapshot.value as? Dictionary<String,Any>
            {
                for singleData in dataDic
                {
                    if singleData.key == Auth.auth().currentUser?.uid
                    {
                    let data = singleData.value as? Dictionary<String,String>
                    for item in data!
                    {
                        favourites.append(item.key)
                    }
                    }
                }
                self.delegate?.didRecieveFavorites(favourites: favourites)
            }
        }
    }
    
    
    func getRatings(place:String)
    {
        let ref = Database.database().reference()
        ref.child("ratings").observeSingleEvent(of: .value) { (snapshot) in
            
            if let dataDic = snapshot.value as? Dictionary<String,Any>
            {
                for singleData in dataDic
                {
                    var one : Double = 0
                    var two : Double = 0
                    var three : Double = 0
                    var four : Double = 0
                    var five : Double = 0
                    
                    if singleData.key == place
                    {
                        let data = singleData.value as? Dictionary<String,Any>
                        
                        for item in data!
                        {
                            if let singleRating = item.value as? Double
                            {
                                if singleRating == 1
                                {
                                    one = one + 1
                                }
                                else if singleRating == 2
                                {
                                    two = two + 1
                                }
                                else if singleRating == 3
                                {
                                    three = three + 1
                                }
                                else if singleRating == 4
                                {
                                    four = four + 1
                                }
                                else if singleRating == 5
                                {
                                    five = five + 1
                                }
                            }
                        }
                        
                        let rate = (one*1)+(two*2)+(three*3)+(four*4)+(five*5)
                        let total = (one+two+three+four+five)
                        
                        if total == 0.0 {
                            self.ratingDelegate?.didRecieveRatings(ratings: 0.0)
                            return
                        }
                        
                        let rating : Double = rate/total
                        self.ratingDelegate?.didRecieveRatings(ratings: rating)
                        break
                    }
                }
                
            }
        }
    }
    
    
}

struct PlacesInfoDataModel
{
    var placeName: String?
    var placeDetail: String?
    var placeImages: [UIImage]?
    var placeLocation: String?
    
    init(){}
    
    init(placeName:String,placeDetail:String,placeImages:[UIImage],placeLocation:String)
    {
        self.placeName = placeName
        self.placeDetail = placeDetail
        self.placeImages = placeImages
        self.placeLocation = placeLocation
    }
}

struct RestaurantsInfoDataModel
{
    var name = ""
    var restaurantName: String?
    var restaurantDetail: String?
    var restaurantImages: [UIImage]?
    var restaurantLocation: String?
    var restaurantTimings : String?
    var restaurantAddress : String?
    
    init(name:String){self.name = name}
    
    init(restaurantName:String,restaurantDetail:String,restaurantImages:[UIImage],restaurantLocation:String,restaurantTimings:String,restaurantAddress:String)
    {
        self.restaurantName = restaurantName
        self.restaurantDetail = restaurantDetail
        self.restaurantImages = restaurantImages
        self.restaurantLocation = restaurantLocation
        self.restaurantTimings = restaurantTimings
        self.restaurantAddress = restaurantAddress
    }
}




